<html>
<head><title>Signup page</title>
<script>
function validate(f) {
 if(f.agree.checked == false) {
  alert('You must agree to the conditions before using this service');
  return false;
 } else { 
  return true;
 }
}
</script>
</head>
<body>
<h2>Chemistry Department web proxy service</h2>
<p>
To use this service, you should be aware of the following:
<ul>
<li>All web pages that you access through this proxy server will be proxied through a Department of Chemistry server</li>
<li>All web pages accessed through this service are logged. Specifically, we log: 
<ul>
<li>your username</li>
<li>your IP address</li>
<li>the full address of every web page requested</li>
<li>the time of access</li></ul></li>
<li>We cannot guarantee that all pages accessed through this service will display or function correctly; if you find that they do not, please instead use an alternative access mechanism (such as our <a href="http://www.ch.cam.ac.uk/computing/openvpn-service">OpenVPN Service</a>)</li>
</ul>
<form action="signup_confirm.php" method="post" onsubmit="return validate(this)">
I agree to the above: <input type="checkbox" value="0" name="agree">
<input type="submit" value="Submit">
</form>
<hr />
<p>Service provided by <a href="http://www.ch.cam.ac.uk/computing">Chemistry Department Computing</a></p>

</body>
</html>

